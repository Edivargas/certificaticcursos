
package com.cinearte.app.client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.cinearte.app.client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Dummy_QNAME = new QName("http://ejb.app.cinearte.com/", "dummy");
    private final static QName _CreateDummyResponse_QNAME = new QName("http://ejb.app.cinearte.com/", "createDummyResponse");
    private final static QName _CreateDummy_QNAME = new QName("http://ejb.app.cinearte.com/", "createDummy");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.cinearte.app.client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Dummy }
     * 
     */
    public Dummy createDummy() {
        return new Dummy();
    }

    /**
     * Create an instance of {@link CreateDummyResponse }
     * 
     */
    public CreateDummyResponse createCreateDummyResponse() {
        return new CreateDummyResponse();
    }

    /**
     * Create an instance of {@link CreateDummy }
     * 
     */
    public CreateDummy createCreateDummy() {
        return new CreateDummy();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Dummy }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.app.cinearte.com/", name = "dummy")
    public JAXBElement<Dummy> createDummy(Dummy value) {
        return new JAXBElement<Dummy>(_Dummy_QNAME, Dummy.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateDummyResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.app.cinearte.com/", name = "createDummyResponse")
    public JAXBElement<CreateDummyResponse> createCreateDummyResponse(CreateDummyResponse value) {
        return new JAXBElement<CreateDummyResponse>(_CreateDummyResponse_QNAME, CreateDummyResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateDummy }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.app.cinearte.com/", name = "createDummy")
    public JAXBElement<CreateDummy> createCreateDummy(CreateDummy value) {
        return new JAXBElement<CreateDummy>(_CreateDummy_QNAME, CreateDummy.class, null, value);
    }

}

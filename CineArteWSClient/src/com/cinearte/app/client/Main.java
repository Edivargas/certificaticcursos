/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cinearte.app.client;

/**
 *
 * @author edivargas
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Dummy dummy = new Dummy();
        dummy.setId(0);
        dummy.setName("Jorge");
        createDummy(dummy);
        // TODO code application logic here
    }

    private static void createDummy(com.cinearte.app.client.Dummy arg0) {
        com.cinearte.app.client.DummyEJBService service = new com.cinearte.app.client.DummyEJBService();
        com.cinearte.app.client.DummyEJB port = service.getDummyEJBPort();
        port.createDummy(arg0);
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cinearte.app.mb;

import com.cinearte.app.cdi.DibujaSalaCDI;
import com.cinearte.app.dto.AsientoDTO;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Axel
 */
@Named("salaMB")
@SessionScoped
public class SalaMB implements Serializable {

    @Inject
    DibujaSalaCDI dibujaSala;
    private int idFuncion;
    private DataModel dataTable;

    public void preparaSala() {
        dibujaSala.init(idFuncion);
        dataTable = new ListDataModel(getMapaSala());
    }

    public void selectAsiento(AsientoDTO asiento) {
        System.out.println("Vamos a usar este asiento:" + asiento);
    }
    
    public void preparaSalaByIdFuncion(int idFuncion) {
        this.idFuncion = idFuncion;
        dibujaSala.init(idFuncion);
        dataTable = new ListDataModel(getMapaSala());
    }

    public DataModel getDataTable() {
        return dataTable;
    }

    public void setDataTable(DataModel dataTable) {
        this.dataTable = dataTable;
    }

    public int getIdFuncion() {
        return idFuncion;
    }

    public void setIdFuncion(int idFuncion) {
        this.idFuncion = idFuncion;
    }

    public List<List<AsientoDTO>> getMapaSala() {
        return dibujaSala.getMapaSala();
    }

}

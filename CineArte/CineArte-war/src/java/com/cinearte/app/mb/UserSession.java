/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cinearte.app.mb;

import com.cinearte.app.bean.Pelicula;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 *
 * @author edivargas
 */
@SessionScoped
@Named("userSession")
public class UserSession implements Serializable {

    private UIInput passwordInput;
    private String user;
    private String password;
    private String selected;
    private String selectedPage;
    private boolean signed;
    private String theme = "dark";
    private Locale localLocale;
    private String rfc;
    private List<Pelicula> peliculas = new ArrayList<>();

    @PostConstruct
    public void setDefaultLocale() {
        localLocale = FacesContext.getCurrentInstance().getApplication().getDefaultLocale();
        if (FacesContext.getCurrentInstance().getViewRoot() != null) {
            FacesContext.getCurrentInstance().getViewRoot().setLocale(localLocale);
        }
        for (int i = 0; i < 10; i++) {
            Pelicula pelicula = new Pelicula();
            pelicula.setIdPelicula(i);
            pelicula.setNombrePelicula("Nombre " + i);
            pelicula.setDuracion(new Random().nextInt(120));
            pelicula.setAnio(1990 + i);
            peliculas.add(pelicula);
        }

    }

    public void setLanguage(String lang) {
        localLocale = new Locale(lang);
        FacesContext.getCurrentInstance()
                .getViewRoot().setLocale(localLocale);
    }

    public Locale getLocalLocale() {
        return localLocale;
    }

    public void setLocalLocale(Locale localLocale) {
        this.localLocale = localLocale;
    }

    public String getSelected() {
        return selected;
    }

    public void setSelected(String selected) {
        this.selected = selected;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public UIInput getPasswordInput() {
        return passwordInput;
    }

    public void setPasswordInput(UIInput passwordInput) {
        this.passwordInput = passwordInput;
    }

    public String getPassword() {
        //return passwordInput == null ? null : 
        //        passwordInput.getLocalValue() == null
        //        ? null : passwordInput.getLocalValue().toString();
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSelectedPage() {
        return selectedPage;
    }

    public void setSelectedPage(String selectedPage) {
        this.selectedPage = selectedPage;
    }

    public boolean isSigned() {
        return signed;
    }

    public void setSigned(boolean signed) {
        this.signed = signed;
    }

    public String getRfc() {
        System.out.println("RFC:" + rfc);
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public List<Pelicula> getPeliculas() {
        return peliculas;
    }

    public void setPeliculas(List<Pelicula> peliculas) {
        this.peliculas = peliculas;
    }
}

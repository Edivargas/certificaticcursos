/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cinearte.app.mb;

import com.cinearte.app.ejb.CompraEJB;
import java.io.Serializable;
import java.util.Date;
import java.util.Random;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author edivargas
 */
@Named(value = "utilMB")
@RequestScoped
public class UtilMB implements Serializable {

    @EJB
    CompraEJB compraEJB;
    @Inject
    UserSession userSession;

    // private String theme = "styled-select"; for v1
    /**
     * Creates a new instance of UtilMB
     */
    public UtilMB() {
    }

    @PostConstruct
    public void selectTheme() {
        // Para index_v1.xhtml descomentar las sig lineas
        //if (new Random().nextBoolean()) {
        //    theme = "other";
        //}
        // Para ese ejercicio es necesario mantener 
        // una variable theme en esta clase
    }

    public String getFecha() {
        return new Date().toString();
    }

    public void procesaDatosListener(ActionEvent event) {
        System.out.println("Desde el Listener procesaDatosListener");
        System.out.println("User:" + userSession.getUser()
                + ":Password:" + userSession.getPassword());
        compraEJB.compra();
    }

    public String procesaDatos() {
        System.out.println("Desde el Action procesaDatosListener");
        return userSession.getSelectedPage();
        //return null;
    }

    public void signin(ActionEvent event) {
        if (userSession.getUser().equals(userSession.getPassword())) {
            userSession.setSigned(true);
        } else {
            userSession.setSigned(false);
        }
    }

    public void signoff(ActionEvent event) {
        userSession.setSigned(false);
    }

    public String navega(String origen) {
        if ("login".equals(origen)) {
            if (userSession.isSigned()) {
                return "/menu";
            } else {
                return null;
            }
        } else {
            if (userSession.isSigned()) {
                return "/menu";
            }
        }
        return "/login";
    }

    public void changeValue(ValueChangeEvent event) {
        System.out.println("Evento:" + event);
        //Registra el cambio
    }

    public boolean isRendered() {
        return new Random().nextBoolean();
    }

}

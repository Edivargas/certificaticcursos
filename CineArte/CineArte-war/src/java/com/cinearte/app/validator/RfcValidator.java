/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cinearte.app.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author edivargas
 */
@FacesValidator("rfcValidator")
public class RfcValidator implements Validator {

    @Override
    public void validate(FacesContext context, 
            UIComponent component, 
            Object value) throws ValidatorException {
        String valueStr = "";
        if (value != null) {
            valueStr = value.toString();
        }
        if (valueStr.length() != 12 && valueStr.length() != 13) {
            FacesMessage message = new FacesMessage("Que te pasa!!");
            throw new ValidatorException(message);
        }
        if (valueStr.matches("~#$^()=:;,./<>+/")) {
            FacesMessage message = new FacesMessage("Solo caracteres validos");
            throw new ValidatorException(message);
        }
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cinearte.app.cdi;

import com.cinearte.app.bean.Boleto;
import com.cinearte.app.bean.ConfiguracionAsientoSala;
import com.cinearte.app.bean.Funcion;
import com.cinearte.app.dao.BoletoDAO;
import com.cinearte.app.dao.ConfiguracionAsientoSalaDAO;
import com.cinearte.app.dao.FuncionDAO;
import com.cinearte.app.dto.AsientoDTO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.inject.Inject;
import javax.inject.Named;
import javax.enterprise.context.Dependent;

/**
 *
 * @author Axel
 */
@Dependent
@Named("dibujaSala")
public class DibujaSalaCDI implements Serializable {

    public static final int ASIENTO_VACIO = 0;
    public static final int ASIENTO_VENDIDO = 1;
    public static final int ASIENTO_RESERVADO = 2;
    public static final int ASIENTO_PASILLO = 3;
    public static final int ASIENTO_MINUSVALIDO = 4;
    private ConfiguracionAsientoSala configuracionAsientoSala;
    private Funcion funcion;
    private List<Boleto> boletosVendidos;
    private final Set<Integer> pasillosHorizontales = new HashSet<>();
    private final Set<Integer> pasillosVerticales = new HashSet<>();
    private final Set<String> boletosVendidosSet = new HashSet<>();
    private final Set<String> boletosReservadosSet = new HashSet<>();
    private final List<List<AsientoDTO>> mapaSala = new ArrayList<>();

    @Inject
    ConfiguracionAsientoSalaDAO configuracionAsientoSalaDAO;
    @Inject
    FuncionDAO funcionDAO;
    @Inject
    BoletoDAO boletoDAO;

    public void init(int idFuncion) {
        this.funcion = funcionDAO.find(idFuncion);
        this.configuracionAsientoSala = configuracionAsientoSalaDAO.getByIdSala(funcion.getIdSala().getIdSala());
        this.boletosVendidos = boletoDAO.getByFuncion(idFuncion);
        Arrays.asList(configuracionAsientoSala.getPasillosH().split(",")).forEach(s -> pasillosHorizontales.add(Integer.parseInt(s)));
        Arrays.asList(configuracionAsientoSala.getPasillosV().split(",")).forEach(s -> pasillosVerticales.add(Integer.parseInt(s)));
        //pasillosVerticales.addAll(Arrays.asList(configuracionAsientoSala.getPasillosV().split(",")));
//        for (Boleto boletosVendido : boletosVendidos) {
//            boletosVendidosSet.add(boletosVendido.getAsiento());
//        }
        boletosVendidos.forEach(boleto -> {
            if (boleto.getStatus() == 1) {
                boletosVendidosSet.add(boleto.getAsiento());
            } else {
                boletosReservadosSet.add(boleto.getAsiento());
            }
        });
        construyeMapa();
    }

    public List<List<AsientoDTO>> getMapaSala() {
        return Collections.unmodifiableList(mapaSala);
    }

    private void construyeMapa() {
        mapaSala.clear();
        for (int i = 1; i <= configuracionAsientoSala.getFila(); i++) {
            List<AsientoDTO> fila = new ArrayList<>();
            mapaSala.add(fila);
            for (int j = 1; j <= configuracionAsientoSala.getColumna(); j++) {
                AsientoDTO asiento
                        = new AsientoDTO(i, j,
                                getTipoAsiento(i, j),
                                getLeyenda(i, j));
                fila.add(asiento);
                if (pasillosVerticales.contains(j)) {
                    fila.add(new AsientoDTO(i, j,
                            ASIENTO_PASILLO,
                            getLeyenda(i, j)));
                }
            }
            if (pasillosHorizontales.contains(i)) {
                mapaSala.add(createFilaPasillo(i));
            }
        }
        mapaSala.forEach((fila) -> {
            System.out.println("fila :" + ":" + fila);
        });
    }

    private int getTipoAsiento(int fila, int columna) {
        String leyenda = getLeyenda(fila, columna);
        if (boletosVendidosSet.contains(leyenda)) {
            return ASIENTO_VENDIDO;
        } else if (boletosReservadosSet.contains(leyenda)) {
            return ASIENTO_RESERVADO;
        } else {
            return ASIENTO_VACIO;
        }
    }

    public String getLeyenda(int fila, int columna) {
        return new String(new char[]{(char) ((fila - 1) + 'A')}) + columna;
    }

    private List<AsientoDTO> createFilaPasillo(int fila) {
        List<AsientoDTO> filaPasillo = new ArrayList<>();
        for (int i = 0; i < configuracionAsientoSala.getColumna() + pasillosVerticales.size(); i++) {
            AsientoDTO asiento = 
                    new AsientoDTO(fila, i, 
                            ASIENTO_PASILLO, "  ");
            filaPasillo.add(asiento);
        }
        return filaPasillo;
    }

    public static void main(String[] args) {
        DibujaSalaCDI dibujaSalaEJB = new DibujaSalaCDI();
        System.out.println(dibujaSalaEJB.getLeyenda(1, 1));
        System.out.println(dibujaSalaEJB.getLeyenda(8, 4));
        System.out.println(dibujaSalaEJB.getLeyenda(19, 1));
        System.out.println(dibujaSalaEJB.getLeyenda(13, 9));
    }

}

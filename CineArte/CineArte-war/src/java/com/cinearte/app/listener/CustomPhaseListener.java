/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cinearte.app.listener;

import com.cinearte.app.mb.UserSession;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

/**
 *
 * @author Axel
 */
public class CustomPhaseListener implements PhaseListener {

    @Override
    public void afterPhase(PhaseEvent event) {
        
    }

    @Override
    public void beforePhase(PhaseEvent event) {
        System.out.println("Iniciando Phase listener...");
        UserSession userSession = FacesContext.getCurrentInstance().getApplication().
                evaluateExpressionGet(FacesContext.getCurrentInstance(),"#{userSession}", UserSession.class);
        if(userSession != null){
            FacesContext.getCurrentInstance().getViewRoot().setLocale(userSession.getLocalLocale());
        }
    }

    @Override
    public PhaseId getPhaseId() {
        return PhaseId.RENDER_RESPONSE;
    }
    
}

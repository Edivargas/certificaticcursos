/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cinearte.app.converter;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author edivargas
 */
@FacesConverter("rfcConverter")
public class ConvertRFC implements Converter {

    @Override
    public Object getAsObject(FacesContext context,
            UIComponent component, String value) {
        if (value == null || value.isEmpty()) {
            System.out.println("ID: " + component.getId());
            FacesContext.getCurrentInstance()
                    .addMessage(component.getParent().getId()
                            + ":" + component.getId(),
                            new FacesMessage("Cuida que si vaya"));
            value = "XEXX010101000";
        }
        return value.toUpperCase().replace("-", "").replace(" ", "");
    }

    @Override
    public String getAsString(FacesContext context,
            UIComponent component, Object value) {
        String valueStr = (String) value;
        if (valueStr == null || valueStr.isEmpty()) {
            valueStr = "XEXX010101000";
        }
        if (valueStr.length() == 13) {
            return valueStr.toUpperCase().substring(0, 4)
                    + "-" + valueStr.substring(4, 10)
                    + "-" + valueStr.substring(10);
        } else {
            return valueStr.toUpperCase().substring(0, 3)
                    + "-" + valueStr.substring(3, 9)
                    + "-" + valueStr.substring(9);

        }
    }
}

-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema cine_arte
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema cine_arte
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `cine_arte` DEFAULT CHARACTER SET utf8 ;
USE `cine_arte` ;

-- -----------------------------------------------------
-- Table `cine_arte`.`Actor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cine_arte`.`Actor` (
  `idActor` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `nombre` VARCHAR(45) NULL DEFAULT NULL COMMENT '',
  `edad` INT NULL DEFAULT NULL COMMENT '',
  `sexo` CHAR(1) NULL DEFAULT NULL COMMENT '',
  `foto` VARCHAR(100) NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (`idActor`)  COMMENT '')
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `cine_arte`.`Usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cine_arte`.`Usuario` (
  `idUsuario` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `nombre` VARCHAR(45) NULL DEFAULT NULL COMMENT '',
  `edad` INT(11) NULL DEFAULT NULL COMMENT '',
  `correo` VARCHAR(100) NULL DEFAULT NULL COMMENT '',
  `direccion` VARCHAR(100) NULL DEFAULT NULL COMMENT '',
  `contrasenia` VARCHAR(45) NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (`idUsuario`)  COMMENT '')
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = '     ';


-- -----------------------------------------------------
-- Table `cine_arte`.`Forma_Pago`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cine_arte`.`Forma_Pago` (
  `idFormaPago` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `idUsuario` INT(11) NOT NULL COMMENT '',
  `idTipoFormaPago` INT(11) NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (`idFormaPago`)  COMMENT '',
  INDEX `fk_Forma_Pago_Usuario1_idx` (`idUsuario` ASC)  COMMENT '')
ENGINE = MyISAM
DEFAULT CHARACTER SET = utf8
COMMENT = ' \n\n';


-- -----------------------------------------------------
-- Table `cine_arte`.`Compra`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cine_arte`.`Compra` (
  `idCompra` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `idUsuario` INT(11) NULL DEFAULT NULL COMMENT '',
  `fecha` DATE NULL DEFAULT NULL COMMENT '',
  `idFormaPago` INT(11) NOT NULL COMMENT '',
  PRIMARY KEY (`idCompra`)  COMMENT '',
  INDEX `fk_Compra_Usuario1_idx` (`idUsuario` ASC)  COMMENT '',
  INDEX `fk_Compra_Forma_Pago1_idx` (`idFormaPago` ASC)  COMMENT '',
  CONSTRAINT `fk_Compra_Usuario1`
    FOREIGN KEY (`idUsuario`)
    REFERENCES `cine_arte`.`Usuario` (`idUsuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Compra_Forma_Pago1`
    FOREIGN KEY (`idFormaPago`)
    REFERENCES `cine_arte`.`Forma_Pago` (`idFormaPago`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `cine_arte`.`Director`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cine_arte`.`Director` (
  `idDirector` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `nombre` VARCHAR(45) NULL DEFAULT NULL COMMENT '',
  `edad` VARCHAR(45) NULL DEFAULT NULL COMMENT '',
  `sexo` CHAR(1) NULL DEFAULT NULL COMMENT '',
  `foto` VARCHAR(100) NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (`idDirector`)  COMMENT '')
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `cine_arte`.`Genero`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cine_arte`.`Genero` (
  `idGenero` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `descripcion` VARCHAR(45) NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (`idGenero`)  COMMENT '')
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `cine_arte`.`Pelicula`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cine_arte`.`Pelicula` (
  `idPelicula` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `nombrePelicula` VARCHAR(45) NULL DEFAULT NULL COMMENT '',
  `duracion` INT(11) NULL DEFAULT NULL COMMENT '',
  `clasificacion` VARCHAR(3) NULL DEFAULT NULL COMMENT '',
  `sinopsis` VARCHAR(500) NULL DEFAULT NULL COMMENT '',
  `anio` INT(11) NULL DEFAULT NULL COMMENT '',
  `urlSitioWeb` VARCHAR(100) NULL DEFAULT NULL COMMENT '',
  `urlTrailer` VARCHAR(45) NULL DEFAULT NULL COMMENT '',
  `thumbnail` VARCHAR(100) NULL DEFAULT NULL COMMENT '',
  `poster` VARCHAR(100) NULL DEFAULT NULL COMMENT '',
  `idDirector` INT(11) NULL DEFAULT NULL COMMENT '',
  `idGenero` INT(11) NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (`idPelicula`)  COMMENT '',
  INDEX `fk_Pelicula_Director1_idx` (`idDirector` ASC)  COMMENT '',
  INDEX `fk_Pelicula_Genero1_idx` (`idGenero` ASC)  COMMENT '',
  CONSTRAINT `fk_Pelicula_Director1`
    FOREIGN KEY (`idDirector`)
    REFERENCES `cine_arte`.`Director` (`idDirector`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Pelicula_Genero1`
    FOREIGN KEY (`idGenero`)
    REFERENCES `cine_arte`.`Genero` (`idGenero`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `cine_arte`.`Cine`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cine_arte`.`Cine` (
  `idCine` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `nombre` VARCHAR(45) NULL DEFAULT NULL COMMENT '',
  `telefono` VARCHAR(45) NULL DEFAULT NULL COMMENT '',
  `horario` VARCHAR(45) NULL DEFAULT NULL COMMENT '',
  `cp` INT(11) NULL DEFAULT NULL COMMENT '',
  `latitud` DECIMAL(9,6) NULL DEFAULT NULL COMMENT '',
  `longitud` DECIMAL(9,6) NULL COMMENT '',
  PRIMARY KEY (`idCine`)  COMMENT '')
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `cine_arte`.`Tipo_Sala`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cine_arte`.`Tipo_Sala` (
  `idTipoSala` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `descripcion` VARCHAR(45) NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (`idTipoSala`)  COMMENT '')
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `cine_arte`.`Sala`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cine_arte`.`Sala` (
  `idSala` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `idCine` INT(11) NULL DEFAULT NULL COMMENT '',
  `numSala` INT(11) NULL DEFAULT NULL COMMENT '',
  `cupo` INT(11) NULL DEFAULT NULL COMMENT '',
  `idTipoSala` INT(11) NULL DEFAULT NULL COMMENT '',
  `estado` INT(11) NULL DEFAULT NULL COMMENT '',
  `filas` INT(11) NULL DEFAULT NULL COMMENT '',
  `columnas` INT(11) NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (`idSala`)  COMMENT '',
  INDEX `fk_Sala_Cine1_idx` (`idCine` ASC)  COMMENT '',
  INDEX `fk_Sala_Tipo_Sala1_idx` (`idTipoSala` ASC)  COMMENT '',
  CONSTRAINT `fk_Sala_Cine1`
    FOREIGN KEY (`idCine`)
    REFERENCES `cine_arte`.`Cine` (`idCine`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Sala_Tipo_Sala1`
    FOREIGN KEY (`idTipoSala`)
    REFERENCES `cine_arte`.`Tipo_Sala` (`idTipoSala`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `cine_arte`.`Tipo_Funcion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cine_arte`.`Tipo_Funcion` (
  `idTipoFuncion` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `descripcion` VARCHAR(45) NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (`idTipoFuncion`)  COMMENT '')
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `cine_arte`.`Funcion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cine_arte`.`Funcion` (
  `idFuncion` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `fecha` DATE NULL DEFAULT NULL COMMENT '',
  `idPelicula` INT(11) NULL DEFAULT NULL COMMENT '',
  `idSala` INT(11) NULL DEFAULT NULL COMMENT '',
  `idTipoFuncion` INT(11) NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (`idFuncion`)  COMMENT '',
  INDEX `fk_Funcion_Tipo_Funcion1_idx` (`idTipoFuncion` ASC)  COMMENT '',
  INDEX `fk_Funcion_Pelicula1_idx` (`idPelicula` ASC)  COMMENT '',
  INDEX `fk_Funcion_Sala2` (`idSala` ASC)  COMMENT '',
  CONSTRAINT `fk_Funcion_Pelicula1`
    FOREIGN KEY (`idPelicula`)
    REFERENCES `cine_arte`.`Pelicula` (`idPelicula`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Funcion_Sala2`
    FOREIGN KEY (`idSala`)
    REFERENCES `cine_arte`.`Sala` (`idSala`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Funcion_Tipo_Funcion1`
    FOREIGN KEY (`idTipoFuncion`)
    REFERENCES `cine_arte`.`Tipo_Funcion` (`idTipoFuncion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `cine_arte`.`Boleto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cine_arte`.`Boleto` (
  `idBoleto` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `idFuncion` INT(11) NULL DEFAULT NULL COMMENT '',
  `asiento` VARCHAR(5) NULL DEFAULT NULL COMMENT '',
  `idCompra` INT(11) NULL DEFAULT NULL COMMENT '',
  `idTipoBoleto` INT(11) NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (`idBoleto`)  COMMENT '',
  INDEX `fk_Boleto_Funcion1_idx` (`idFuncion` ASC)  COMMENT '',
  INDEX `fk_Boleto_Compra1_idx` (`idCompra` ASC)  COMMENT '',
  CONSTRAINT `fk_Boleto_Compra1`
    FOREIGN KEY (`idCompra`)
    REFERENCES `cine_arte`.`Compra` (`idCompra`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Boleto_Funcion1`
    FOREIGN KEY (`idFuncion`)
    REFERENCES `cine_arte`.`Funcion` (`idFuncion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = ' ';


-- -----------------------------------------------------
-- Table `cine_arte`.`Comentario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cine_arte`.`Comentario` (
  `idComentario` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `idPelicula` INT(11) NULL DEFAULT NULL COMMENT '',
  `idUsuario` INT(11) NULL DEFAULT NULL COMMENT '',
  `comentario` VARCHAR(100) NULL DEFAULT NULL COMMENT '',
  `calificacion` INT(11) NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (`idComentario`)  COMMENT '',
  INDEX `fk_Comentario_Usuario1_idx` (`idUsuario` ASC)  COMMENT '',
  INDEX `fk_Comentario_Pelicula1_idx` (`idPelicula` ASC)  COMMENT '',
  CONSTRAINT `fk_Comentario_Pelicula1`
    FOREIGN KEY (`idPelicula`)
    REFERENCES `cine_arte`.`Pelicula` (`idPelicula`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Comentario_Usuario1`
    FOREIGN KEY (`idUsuario`)
    REFERENCES `cine_arte`.`Usuario` (`idUsuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `cine_arte`.`Configuracion_Asiento_Sala`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cine_arte`.`Configuracion_Asiento_Sala` (
  `idConfiguracionAsientoSala` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `columna` VARCHAR(45) NULL DEFAULT NULL COMMENT '',
  `fila` VARCHAR(45) NULL DEFAULT NULL COMMENT '',
  `pasillosV` VARCHAR(45) NULL DEFAULT NULL COMMENT '',
  `pasillosH` VARCHAR(45) NULL DEFAULT NULL COMMENT '',
  `idSala` INT(11) NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (`idConfiguracionAsientoSala`)  COMMENT '',
  INDEX `fk_Configuracion_Asiento_Sala_Sala1_idx` (`idSala` ASC)  COMMENT '',
  CONSTRAINT `fk_Configuracion_Asiento_Sala_Sala1`
    FOREIGN KEY (`idSala`)
    REFERENCES `cine_arte`.`Sala` (`idSala`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `cine_arte`.`Tipo_Boleto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cine_arte`.`Tipo_Boleto` (
  `idTipoBoleto` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `descripcion` VARCHAR(45) NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (`idTipoBoleto`)  COMMENT '',
  CONSTRAINT `idTipoBoleto`
    FOREIGN KEY (`idTipoBoleto`)
    REFERENCES `cine_arte`.`Boleto` (`idBoleto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `cine_arte`.`Funcion_Tipo_Boleto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cine_arte`.`Funcion_Tipo_Boleto` (
  `idFuncionTipoBoleto` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `idFuncion` INT(11) NULL DEFAULT NULL COMMENT '',
  `idTipoBoleto` INT(11) NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (`idFuncionTipoBoleto`)  COMMENT '',
  INDEX `fk_Funcion_Tipo_Boleto_Funcion1_idx` (`idFuncion` ASC)  COMMENT '',
  INDEX `fk_Funcion_Tipo_Boleto_Tipo_Boleto1_idx` (`idTipoBoleto` ASC)  COMMENT '',
  CONSTRAINT `fk_Funcion_Tipo_Boleto_Funcion1`
    FOREIGN KEY (`idFuncion`)
    REFERENCES `cine_arte`.`Funcion` (`idFuncion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Funcion_Tipo_Boleto_Tipo_Boleto1`
    FOREIGN KEY (`idTipoBoleto`)
    REFERENCES `cine_arte`.`Tipo_Boleto` (`idTipoBoleto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `cine_arte`.`Pelicula_Actor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cine_arte`.`Pelicula_Actor` (
  `idPeliculaActor` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `idPelicula` INT(11) NULL DEFAULT NULL COMMENT '',
  `idActor` INT(11) NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (`idPeliculaActor`)  COMMENT '',
  INDEX `fk_Pelicula_Actor_Pelicula1_idx` (`idPelicula` ASC)  COMMENT '',
  INDEX `fk_Pelicula_Actor_Actor1_idx` (`idActor` ASC)  COMMENT '',
  CONSTRAINT `fk_Pelicula_Actor_Actor1`
    FOREIGN KEY (`idActor`)
    REFERENCES `cine_arte`.`Actor` (`idActor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Pelicula_Actor_Pelicula1`
    FOREIGN KEY (`idPelicula`)
    REFERENCES `cine_arte`.`Pelicula` (`idPelicula`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `cine_arte`.`Precio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cine_arte`.`Precio` (
  `idPrecio` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `idTipoSala` INT(11) NULL DEFAULT NULL COMMENT '',
  `idTipoBoleto` INT(11) NULL DEFAULT NULL COMMENT '',
  `precio` INT(11) NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (`idPrecio`)  COMMENT '',
  INDEX `fk_Precio_Tipo_Sala1_idx` (`idTipoSala` ASC)  COMMENT '',
  INDEX `fk_Precio_Tipo_Boleto1_idx` (`idTipoBoleto` ASC)  COMMENT '',
  CONSTRAINT `fk_Precio_Tipo_Boleto1`
    FOREIGN KEY (`idTipoBoleto`)
    REFERENCES `cine_arte`.`Tipo_Boleto` (`idTipoBoleto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Precio_Tipo_Sala1`
    FOREIGN KEY (`idTipoSala`)
    REFERENCES `cine_arte`.`Tipo_Sala` (`idTipoSala`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `cine_arte`.`Tipo_Forma_Pago`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cine_arte`.`Tipo_Forma_Pago` (
  `idTipoFormaPago` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `descripcion` VARCHAR(45) NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (`idTipoFormaPago`)  COMMENT '',
  CONSTRAINT `fk_Tipo_Forma_Pago_Forma_Pago1`
    FOREIGN KEY (`idTipoFormaPago`)
    REFERENCES `cine_arte`.`Forma_Pago` (`idTipoFormaPago`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

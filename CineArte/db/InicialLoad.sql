/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  edivargas
 * Created: Oct 5, 2019
 */

INSERT INTO `cine_arte`.`Tipo_Sala` (`idTipoSala`, `descripcion`) VALUES (0, "Tradicional");
INSERT INTO `cine_arte`.`Tipo_Sala` (`idTipoSala`, `descripcion`) VALUES (0, "VIP");
INSERT INTO `cine_arte`.`Tipo_Sala` (`idTipoSala`, `descripcion`) VALUES (0, "Junior");

INSERT INTO `cine_arte`.`Tipo_Boleto` (`idTipoBoleto`, `descripcion`) VALUES (0, "Adulto");
INSERT INTO `cine_arte`.`Tipo_Boleto` (`idTipoBoleto`, `descripcion`) VALUES (0, "Niño");
INSERT INTO `cine_arte`.`Tipo_Boleto` (`idTipoBoleto`, `descripcion`) VALUES (0, "Matinee");
INSERT INTO `cine_arte`.`Tipo_Boleto` (`idTipoBoleto`, `descripcion`) VALUES (0, "Evento Privado");

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cinearte.app.test.ws;

import com.cinearte.app.dto.ResultadoDTO;
import com.cinearte.app.ejb.ActorEJB;
import com.cinearte.app.ejb.BoletoEJB;
import com.cinearte.app.ejb.ComentarioEJB;
import com.cinearte.app.ejb.TipoSalaEJB;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 *
 * @author edivargas
 */
@Stateless
@WebService
public class TestCatalogo implements Serializable {

    @EJB
    TipoSalaEJB tipoSalaEJB;

    @EJB
    ActorEJB actorEJB;

    @EJB
    BoletoEJB boletoEJB;

    @EJB
    ComentarioEJB comentarioEJB;

    @PostConstruct
    public void init() {
        System.out.println("PostCosntruct TestCatalogo");
    }

    @WebMethod
    public ResultadoDTO agregaTipoSala(String descripcion) {
        return tipoSalaEJB.agrega(descripcion);
    }

    @WebMethod
    public ResultadoDTO modificaTipoSala(Long id, String descripcion) {
        return tipoSalaEJB.modifica(id, descripcion);
    }

    @WebMethod
    public ResultadoDTO eliminaTipoSala(Long id) {
        return tipoSalaEJB.elimina(id);
    }

    @WebMethod
    public ResultadoDTO agregaActor(String nombre, Integer edad, String sexo, String foto) {
        return actorEJB.agrega(nombre, edad, sexo, foto);
    }

    @WebMethod
    public ResultadoDTO modificaActor(Long id, String nombre, Integer edad, String sexo, String foto) {
        return actorEJB.modifica(id, nombre, edad, sexo, foto);
    }

    @WebMethod
    public ResultadoDTO elimminaActor(Long id) {
        return actorEJB.elimina(id);
    }

    @WebMethod
    public ResultadoDTO agregaBoleto(String asiento) {
        return boletoEJB.agrega(asiento);
    }

    @WebMethod
    public ResultadoDTO modificaBoleto(Long id, String asiento) {
        return boletoEJB.modifica(id, asiento);
    }

    @WebMethod
    public ResultadoDTO eliminaBoleto(Long id) {
        return boletoEJB.elimina(id);
    }

    @WebMethod
    public ResultadoDTO agregaComentario(String comentario, int calificacion) {
        return comentarioEJB.agrega(comentario, calificacion);
    }
}

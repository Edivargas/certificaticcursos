/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cinearte.app.bean;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author edivargas
 */
@Entity
@Table(name = "configuracion_asiento_sala")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ConfiguracionAsientoSala.findAll", query = "SELECT c FROM ConfiguracionAsientoSala c")
    , @NamedQuery(name = "ConfiguracionAsientoSala.findByIdConfiguracionAsientoSala", query = "SELECT c FROM ConfiguracionAsientoSala c WHERE c.idConfiguracionAsientoSala = :idConfiguracionAsientoSala")
    , @NamedQuery(name = "ConfiguracionAsientoSala.findByColumna", query = "SELECT c FROM ConfiguracionAsientoSala c WHERE c.columna = :columna")
    , @NamedQuery(name = "ConfiguracionAsientoSala.findByFila", query = "SELECT c FROM ConfiguracionAsientoSala c WHERE c.fila = :fila")
    , @NamedQuery(name = "ConfiguracionAsientoSala.findByPasillosV", query = "SELECT c FROM ConfiguracionAsientoSala c WHERE c.pasillosV = :pasillosV")
    , @NamedQuery(name = "ConfiguracionAsientoSala.findByPasillosH", query = "SELECT c FROM ConfiguracionAsientoSala c WHERE c.pasillosH = :pasillosH")})
public class ConfiguracionAsientoSala implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idConfiguracionAsientoSala")
    private Integer idConfiguracionAsientoSala;
    @Size(max = 45)
    @Column(name = "columna")
    private int columna;
    @Size(max = 45)
    @Column(name = "fila")
    private int fila;
    @Size(max = 45)
    @Column(name = "pasillosV")
    private String pasillosV;
    @Size(max = 45)
    @Column(name = "pasillosH")
    private String pasillosH;
    @JoinColumn(name = "idSala", referencedColumnName = "idSala")
    @ManyToOne(fetch = FetchType.EAGER)
    private Sala idSala;

    public ConfiguracionAsientoSala() {
    }

    public ConfiguracionAsientoSala(Integer idConfiguracionAsientoSala) {
        this.idConfiguracionAsientoSala = idConfiguracionAsientoSala;
    }

    public Integer getIdConfiguracionAsientoSala() {
        return idConfiguracionAsientoSala;
    }

    public void setIdConfiguracionAsientoSala(Integer idConfiguracionAsientoSala) {
        this.idConfiguracionAsientoSala = idConfiguracionAsientoSala;
    }

    public int getColumna() {
        return columna;
    }

    public void setColumna(int columna) {
        this.columna = columna;
    }

    public int getFila() {
        return fila;
    }

    public void setFila(int fila) {
        this.fila = fila;
    }

    public String getPasillosV() {
        return pasillosV;
    }

    public void setPasillosV(String pasillosV) {
        this.pasillosV = pasillosV;
    }

    public String getPasillosH() {
        return pasillosH;
    }

    public void setPasillosH(String pasillosH) {
        this.pasillosH = pasillosH;
    }

    public Sala getIdSala() {
        return idSala;
    }

    public void setIdSala(Sala idSala) {
        this.idSala = idSala;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idConfiguracionAsientoSala != null ? idConfiguracionAsientoSala.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConfiguracionAsientoSala)) {
            return false;
        }
        ConfiguracionAsientoSala other = (ConfiguracionAsientoSala) object;
        if ((this.idConfiguracionAsientoSala == null && other.idConfiguracionAsientoSala != null) || (this.idConfiguracionAsientoSala != null && !this.idConfiguracionAsientoSala.equals(other.idConfiguracionAsientoSala))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cinearte.app.bean.ConfiguracionAsientoSala[ idConfiguracionAsientoSala=" + idConfiguracionAsientoSala + " ]";
    }
    
}

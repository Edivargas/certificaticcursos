/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cinearte.app.bean;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author edivargas
 */
@Entity
@Table(name = "pelicula")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pelicula.findAll", query = "SELECT p FROM Pelicula p")
    , @NamedQuery(name = "Pelicula.findByIdPelicula", query = "SELECT p FROM Pelicula p WHERE p.idPelicula = :idPelicula")
    , @NamedQuery(name = "Pelicula.findByNombrePelicula", query = "SELECT p FROM Pelicula p WHERE p.nombrePelicula = :nombrePelicula")
    , @NamedQuery(name = "Pelicula.findByDuracion", query = "SELECT p FROM Pelicula p WHERE p.duracion = :duracion")
    , @NamedQuery(name = "Pelicula.findByClasificacion", query = "SELECT p FROM Pelicula p WHERE p.clasificacion = :clasificacion")
    , @NamedQuery(name = "Pelicula.findBySinopsis", query = "SELECT p FROM Pelicula p WHERE p.sinopsis = :sinopsis")
    , @NamedQuery(name = "Pelicula.findByAnio", query = "SELECT p FROM Pelicula p WHERE p.anio = :anio")
    , @NamedQuery(name = "Pelicula.findByUrlSitioWeb", query = "SELECT p FROM Pelicula p WHERE p.urlSitioWeb = :urlSitioWeb")
    , @NamedQuery(name = "Pelicula.findByUrlTrailer", query = "SELECT p FROM Pelicula p WHERE p.urlTrailer = :urlTrailer")
    , @NamedQuery(name = "Pelicula.findByThumbnail", query = "SELECT p FROM Pelicula p WHERE p.thumbnail = :thumbnail")
    , @NamedQuery(name = "Pelicula.findByPoster", query = "SELECT p FROM Pelicula p WHERE p.poster = :poster")})
public class Pelicula implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idPelicula")
    private Integer idPelicula;
    @Size(max = 45)
    @Column(name = "nombrePelicula")
    private String nombrePelicula;
    @Column(name = "duracion")
    private Integer duracion;
    @Size(max = 3)
    @Column(name = "clasificacion")
    private String clasificacion;
    @Size(max = 500)
    @Column(name = "sinopsis")
    private String sinopsis;
    @Column(name = "anio")
    private Integer anio;
    @Size(max = 100)
    @Column(name = "urlSitioWeb")
    private String urlSitioWeb;
    @Size(max = 45)
    @Column(name = "urlTrailer")
    private String urlTrailer;
    @Size(max = 100)
    @Column(name = "thumbnail")
    private String thumbnail;
    @Size(max = 100)
    @Column(name = "poster")
    private String poster;
    @JoinColumn(name = "idDirector", referencedColumnName = "idDirector")
    @ManyToOne(fetch = FetchType.EAGER)
    private Director idDirector;
    @JoinColumn(name = "idGenero", referencedColumnName = "idGenero")
    @ManyToOne(fetch = FetchType.EAGER)
    private Genero idGenero;
    @OneToMany(mappedBy = "idPelicula", fetch = FetchType.EAGER)
    private List<Comentario> comentarioList;
    @OneToMany(mappedBy = "idPelicula", fetch = FetchType.EAGER)
    private List<PeliculaActor> peliculaActorList;
    @OneToMany(mappedBy = "idPelicula", fetch = FetchType.EAGER)
    private List<Funcion> funcionList;

    public Pelicula() {
    }

    public Pelicula(Integer idPelicula) {
        this.idPelicula = idPelicula;
    }

    public Integer getIdPelicula() {
        return idPelicula;
    }

    public void setIdPelicula(Integer idPelicula) {
        this.idPelicula = idPelicula;
    }

    public String getNombrePelicula() {
        return nombrePelicula;
    }

    public void setNombrePelicula(String nombrePelicula) {
        this.nombrePelicula = nombrePelicula;
    }

    public Integer getDuracion() {
        return duracion;
    }

    public void setDuracion(Integer duracion) {
        this.duracion = duracion;
    }

    public String getClasificacion() {
        return clasificacion;
    }

    public void setClasificacion(String clasificacion) {
        this.clasificacion = clasificacion;
    }

    public String getSinopsis() {
        return sinopsis;
    }

    public void setSinopsis(String sinopsis) {
        this.sinopsis = sinopsis;
    }

    public Integer getAnio() {
        return anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    public String getUrlSitioWeb() {
        return urlSitioWeb;
    }

    public void setUrlSitioWeb(String urlSitioWeb) {
        this.urlSitioWeb = urlSitioWeb;
    }

    public String getUrlTrailer() {
        return urlTrailer;
    }

    public void setUrlTrailer(String urlTrailer) {
        this.urlTrailer = urlTrailer;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public Director getIdDirector() {
        return idDirector;
    }

    public void setIdDirector(Director idDirector) {
        this.idDirector = idDirector;
    }

    public Genero getIdGenero() {
        return idGenero;
    }

    public void setIdGenero(Genero idGenero) {
        this.idGenero = idGenero;
    }

    @XmlTransient
    public List<Comentario> getComentarioList() {
        return comentarioList;
    }

    public void setComentarioList(List<Comentario> comentarioList) {
        this.comentarioList = comentarioList;
    }

    @XmlTransient
    public List<PeliculaActor> getPeliculaActorList() {
        return peliculaActorList;
    }

    public void setPeliculaActorList(List<PeliculaActor> peliculaActorList) {
        this.peliculaActorList = peliculaActorList;
    }

    @XmlTransient
    public List<Funcion> getFuncionList() {
        return funcionList;
    }

    public void setFuncionList(List<Funcion> funcionList) {
        this.funcionList = funcionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPelicula != null ? idPelicula.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pelicula)) {
            return false;
        }
        Pelicula other = (Pelicula) object;
        if ((this.idPelicula == null && other.idPelicula != null) || (this.idPelicula != null && !this.idPelicula.equals(other.idPelicula))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cinearte.app.bean.Pelicula[ idPelicula=" + idPelicula + " ]";
    }
    
}

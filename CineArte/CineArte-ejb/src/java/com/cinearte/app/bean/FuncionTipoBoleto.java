/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cinearte.app.bean;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author edivargas
 */
@Entity
@Table(name = "funcion_tipo_boleto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FuncionTipoBoleto.findAll", query = "SELECT f FROM FuncionTipoBoleto f")
    , @NamedQuery(name = "FuncionTipoBoleto.findByIdFuncionTipoBoleto", query = "SELECT f FROM FuncionTipoBoleto f WHERE f.idFuncionTipoBoleto = :idFuncionTipoBoleto")})
public class FuncionTipoBoleto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idFuncionTipoBoleto")
    private Integer idFuncionTipoBoleto;
    @JoinColumn(name = "idFuncion", referencedColumnName = "idFuncion")
    @ManyToOne(fetch = FetchType.EAGER)
    private Funcion idFuncion;
    @JoinColumn(name = "idTipoBoleto", referencedColumnName = "idTipoBoleto")
    @ManyToOne(fetch = FetchType.EAGER)
    private TipoBoleto idTipoBoleto;

    public FuncionTipoBoleto() {
    }

    public FuncionTipoBoleto(Integer idFuncionTipoBoleto) {
        this.idFuncionTipoBoleto = idFuncionTipoBoleto;
    }

    public Integer getIdFuncionTipoBoleto() {
        return idFuncionTipoBoleto;
    }

    public void setIdFuncionTipoBoleto(Integer idFuncionTipoBoleto) {
        this.idFuncionTipoBoleto = idFuncionTipoBoleto;
    }

    public Funcion getIdFuncion() {
        return idFuncion;
    }

    public void setIdFuncion(Funcion idFuncion) {
        this.idFuncion = idFuncion;
    }

    public TipoBoleto getIdTipoBoleto() {
        return idTipoBoleto;
    }

    public void setIdTipoBoleto(TipoBoleto idTipoBoleto) {
        this.idTipoBoleto = idTipoBoleto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idFuncionTipoBoleto != null ? idFuncionTipoBoleto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FuncionTipoBoleto)) {
            return false;
        }
        FuncionTipoBoleto other = (FuncionTipoBoleto) object;
        if ((this.idFuncionTipoBoleto == null && other.idFuncionTipoBoleto != null) || (this.idFuncionTipoBoleto != null && !this.idFuncionTipoBoleto.equals(other.idFuncionTipoBoleto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cinearte.app.bean.FuncionTipoBoleto[ idFuncionTipoBoleto=" + idFuncionTipoBoleto + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cinearte.app.bean;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author edivargas
 */
@Entity
@Table(name = "sala")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Sala.findAll", query = "SELECT s FROM Sala s")
    , @NamedQuery(name = "Sala.findByIdSala", query = "SELECT s FROM Sala s WHERE s.idSala = :idSala")
    , @NamedQuery(name = "Sala.findByNumSala", query = "SELECT s FROM Sala s WHERE s.numSala = :numSala")
    , @NamedQuery(name = "Sala.findByCupo", query = "SELECT s FROM Sala s WHERE s.cupo = :cupo")
    , @NamedQuery(name = "Sala.findByEstado", query = "SELECT s FROM Sala s WHERE s.estado = :estado")
    , @NamedQuery(name = "Sala.findByFilas", query = "SELECT s FROM Sala s WHERE s.filas = :filas")
    , @NamedQuery(name = "Sala.findByColumnas", query = "SELECT s FROM Sala s WHERE s.columnas = :columnas")})
public class Sala implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idSala")
    private Integer idSala;
    @Column(name = "numSala")
    private Integer numSala;
    @Column(name = "cupo")
    private Integer cupo;
    @Column(name = "estado")
    private Integer estado;
    @Column(name = "filas")
    private Integer filas;
    @Column(name = "columnas")
    private Integer columnas;
    @OneToMany(mappedBy = "idSala", fetch = FetchType.EAGER)
    private List<ConfiguracionAsientoSala> configuracionAsientoSalaList;
    @JoinColumn(name = "idCine", referencedColumnName = "idCine")
    @ManyToOne(fetch = FetchType.EAGER)
    private Cine idCine;
    @JoinColumn(name = "idTipoSala", referencedColumnName = "idTipoSala")
    @ManyToOne(fetch = FetchType.EAGER)
    private TipoSala idTipoSala;
    @OneToMany(mappedBy = "idSala", fetch = FetchType.EAGER)
    private List<Funcion> funcionList;

    public Sala() {
    }

    public Sala(Integer idSala) {
        this.idSala = idSala;
    }

    public Integer getIdSala() {
        return idSala;
    }

    public void setIdSala(Integer idSala) {
        this.idSala = idSala;
    }

    public Integer getNumSala() {
        return numSala;
    }

    public void setNumSala(Integer numSala) {
        this.numSala = numSala;
    }

    public Integer getCupo() {
        return cupo;
    }

    public void setCupo(Integer cupo) {
        this.cupo = cupo;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public Integer getFilas() {
        return filas;
    }

    public void setFilas(Integer filas) {
        this.filas = filas;
    }

    public Integer getColumnas() {
        return columnas;
    }

    public void setColumnas(Integer columnas) {
        this.columnas = columnas;
    }

    @XmlTransient
    public List<ConfiguracionAsientoSala> getConfiguracionAsientoSalaList() {
        return configuracionAsientoSalaList;
    }

    public void setConfiguracionAsientoSalaList(List<ConfiguracionAsientoSala> configuracionAsientoSalaList) {
        this.configuracionAsientoSalaList = configuracionAsientoSalaList;
    }

    public Cine getIdCine() {
        return idCine;
    }

    public void setIdCine(Cine idCine) {
        this.idCine = idCine;
    }

    public TipoSala getIdTipoSala() {
        return idTipoSala;
    }

    public void setIdTipoSala(TipoSala idTipoSala) {
        this.idTipoSala = idTipoSala;
    }

    @XmlTransient
    public List<Funcion> getFuncionList() {
        return funcionList;
    }

    public void setFuncionList(List<Funcion> funcionList) {
        this.funcionList = funcionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSala != null ? idSala.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sala)) {
            return false;
        }
        Sala other = (Sala) object;
        if ((this.idSala == null && other.idSala != null) || (this.idSala != null && !this.idSala.equals(other.idSala))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cinearte.app.bean.Sala[ idSala=" + idSala + " ]";
    }
    
}

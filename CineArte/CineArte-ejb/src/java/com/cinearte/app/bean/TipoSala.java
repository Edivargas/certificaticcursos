/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cinearte.app.bean;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author edivargas
 */
@Entity
@Table(name = "tipo_sala")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoSala.findAll", query = "SELECT t FROM TipoSala t")
    , @NamedQuery(name = "TipoSala.findByIdTipoSala", query = "SELECT t FROM TipoSala t WHERE t.idTipoSala = :idTipoSala")
    , @NamedQuery(name = "TipoSala.findByDescripcion", query = "SELECT t FROM TipoSala t WHERE t.descripcion = :descripcion")})
public class TipoSala implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idTipoSala")
    private Long idTipoSala;
    @Size(max = 45)
    @Column(name = "descripcion")
    private String descripcion;
    @OneToMany(mappedBy = "idTipoSala", fetch = FetchType.EAGER)
    private List<Precio> precioList;
    @OneToMany(mappedBy = "idTipoSala", fetch = FetchType.EAGER)
    private List<Sala> salaList;

    public TipoSala() {
    }

    public TipoSala(Long idTipoSala) {
        this.idTipoSala = idTipoSala;
    }

    public Long getIdTipoSala() {
        return idTipoSala;
    }

    public void setIdTipoSala(Long idTipoSala) {
        this.idTipoSala = idTipoSala;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public List<Precio> getPrecioList() {
        return precioList;
    }

    public void setPrecioList(List<Precio> precioList) {
        this.precioList = precioList;
    }

    @XmlTransient
    public List<Sala> getSalaList() {
        return salaList;
    }

    public void setSalaList(List<Sala> salaList) {
        this.salaList = salaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipoSala != null ? idTipoSala.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoSala)) {
            return false;
        }
        TipoSala other = (TipoSala) object;
        if ((this.idTipoSala == null && other.idTipoSala != null) || (this.idTipoSala != null && !this.idTipoSala.equals(other.idTipoSala))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "TipoSala{" + "idTipoSala=" + idTipoSala + ", descripcion=" + descripcion + '}';
    }

}

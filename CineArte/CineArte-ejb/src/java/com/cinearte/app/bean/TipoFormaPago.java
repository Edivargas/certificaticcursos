/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cinearte.app.bean;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author edivargas
 */
@Entity
@Table(name = "tipo_forma_pago")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoFormaPago.findAll", query = "SELECT t FROM TipoFormaPago t")
    , @NamedQuery(name = "TipoFormaPago.findByIdTipoFormaPago", query = "SELECT t FROM TipoFormaPago t WHERE t.idTipoFormaPago = :idTipoFormaPago")
    , @NamedQuery(name = "TipoFormaPago.findByDescripcion", query = "SELECT t FROM TipoFormaPago t WHERE t.descripcion = :descripcion")})
public class TipoFormaPago implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idTipoFormaPago")
    private Integer idTipoFormaPago;
    @Size(max = 45)
    @Column(name = "descripcion")
    private String descripcion;
    @JoinColumn(name = "idTipoFormaPago", referencedColumnName = "idTipoFormaPago", insertable = false, updatable = false)
    @OneToOne(optional = false, fetch = FetchType.EAGER)
    private FormaPago formaPago;

    public TipoFormaPago() {
    }

    public TipoFormaPago(Integer idTipoFormaPago) {
        this.idTipoFormaPago = idTipoFormaPago;
    }

    public Integer getIdTipoFormaPago() {
        return idTipoFormaPago;
    }

    public void setIdTipoFormaPago(Integer idTipoFormaPago) {
        this.idTipoFormaPago = idTipoFormaPago;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public FormaPago getFormaPago() {
        return formaPago;
    }

    public void setFormaPago(FormaPago formaPago) {
        this.formaPago = formaPago;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipoFormaPago != null ? idTipoFormaPago.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoFormaPago)) {
            return false;
        }
        TipoFormaPago other = (TipoFormaPago) object;
        if ((this.idTipoFormaPago == null && other.idTipoFormaPago != null) || (this.idTipoFormaPago != null && !this.idTipoFormaPago.equals(other.idTipoFormaPago))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cinearte.app.bean.TipoFormaPago[ idTipoFormaPago=" + idTipoFormaPago + " ]";
    }
    
}

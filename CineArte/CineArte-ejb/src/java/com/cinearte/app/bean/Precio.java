/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cinearte.app.bean;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author edivargas
 */
@Entity
@Table(name = "precio")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Precio.findAll", query = "SELECT p FROM Precio p")
    , @NamedQuery(name = "Precio.findByIdPrecio", query = "SELECT p FROM Precio p WHERE p.idPrecio = :idPrecio")
    , @NamedQuery(name = "Precio.findByPrecio", query = "SELECT p FROM Precio p WHERE p.precio = :precio")})
public class Precio implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idPrecio")
    private Integer idPrecio;
    @Column(name = "precio")
    private Integer precio;
    @JoinColumn(name = "idTipoBoleto", referencedColumnName = "idTipoBoleto")
    @ManyToOne(fetch = FetchType.EAGER)
    private TipoBoleto idTipoBoleto;
    @JoinColumn(name = "idTipoSala", referencedColumnName = "idTipoSala")
    @ManyToOne(fetch = FetchType.EAGER)
    private TipoSala idTipoSala;

    public Precio() {
    }

    public Precio(Integer idPrecio) {
        this.idPrecio = idPrecio;
    }

    public Integer getIdPrecio() {
        return idPrecio;
    }

    public void setIdPrecio(Integer idPrecio) {
        this.idPrecio = idPrecio;
    }

    public Integer getPrecio() {
        return precio;
    }

    public void setPrecio(Integer precio) {
        this.precio = precio;
    }

    public TipoBoleto getIdTipoBoleto() {
        return idTipoBoleto;
    }

    public void setIdTipoBoleto(TipoBoleto idTipoBoleto) {
        this.idTipoBoleto = idTipoBoleto;
    }

    public TipoSala getIdTipoSala() {
        return idTipoSala;
    }

    public void setIdTipoSala(TipoSala idTipoSala) {
        this.idTipoSala = idTipoSala;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPrecio != null ? idPrecio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Precio)) {
            return false;
        }
        Precio other = (Precio) object;
        if ((this.idPrecio == null && other.idPrecio != null) || (this.idPrecio != null && !this.idPrecio.equals(other.idPrecio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cinearte.app.bean.Precio[ idPrecio=" + idPrecio + " ]";
    }
    
}

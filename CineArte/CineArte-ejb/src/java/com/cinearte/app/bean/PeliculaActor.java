/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cinearte.app.bean;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author edivargas
 */
@Entity
@Table(name = "pelicula_actor")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PeliculaActor.findAll", query = "SELECT p FROM PeliculaActor p")
    , @NamedQuery(name = "PeliculaActor.findByIdPeliculaActor", query = "SELECT p FROM PeliculaActor p WHERE p.idPeliculaActor = :idPeliculaActor")})
public class PeliculaActor implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idPeliculaActor")
    private Integer idPeliculaActor;
    @JoinColumn(name = "idActor", referencedColumnName = "idActor")
    @ManyToOne(fetch = FetchType.EAGER)
    private Actor idActor;
    @JoinColumn(name = "idPelicula", referencedColumnName = "idPelicula")
    @ManyToOne(fetch = FetchType.EAGER)
    private Pelicula idPelicula;

    public PeliculaActor() {
    }

    public PeliculaActor(Integer idPeliculaActor) {
        this.idPeliculaActor = idPeliculaActor;
    }

    public Integer getIdPeliculaActor() {
        return idPeliculaActor;
    }

    public void setIdPeliculaActor(Integer idPeliculaActor) {
        this.idPeliculaActor = idPeliculaActor;
    }

    public Actor getIdActor() {
        return idActor;
    }

    public void setIdActor(Actor idActor) {
        this.idActor = idActor;
    }

    public Pelicula getIdPelicula() {
        return idPelicula;
    }

    public void setIdPelicula(Pelicula idPelicula) {
        this.idPelicula = idPelicula;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPeliculaActor != null ? idPeliculaActor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PeliculaActor)) {
            return false;
        }
        PeliculaActor other = (PeliculaActor) object;
        if ((this.idPeliculaActor == null && other.idPeliculaActor != null) || (this.idPeliculaActor != null && !this.idPeliculaActor.equals(other.idPeliculaActor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cinearte.app.bean.PeliculaActor[ idPeliculaActor=" + idPeliculaActor + " ]";
    }
    
}

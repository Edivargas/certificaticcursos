/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cinearte.app.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author edivargas
 */
@Entity
@Table(name = "funcion")
@Cacheable(false)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Funcion.findAll", query = "SELECT f FROM Funcion f")
    , @NamedQuery(name = "Funcion.findByIdFuncion", query = "SELECT f FROM Funcion f WHERE f.idFuncion = :idFuncion")
    , @NamedQuery(name = "Funcion.findByFecha", query = "SELECT f FROM Funcion f WHERE f.fecha = :fecha")})
public class Funcion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idFuncion")
    private Integer idFuncion;
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    
    @OneToMany(mappedBy = "idFuncion", fetch = FetchType.EAGER)
    private List<Boleto> boletoList;
    @OneToMany(mappedBy = "idFuncion", fetch = FetchType.EAGER)
    private List<FuncionTipoBoleto> funcionTipoBoletoList;
    @JoinColumn(name = "idPelicula", referencedColumnName = "idPelicula")
    @ManyToOne(fetch = FetchType.EAGER)
    private Pelicula idPelicula;
    @JoinColumn(name = "idSala", referencedColumnName = "idSala")
    @ManyToOne(fetch = FetchType.EAGER)
    private Sala idSala;
    
    @JoinColumn(name = "idTipoFuncion", referencedColumnName = "idTipoFuncion")
    //@ManyToOne(fetch = FetchType.EAGER)
    private TipoFuncion idTipoFuncion;

    public Funcion() {
    }

    public Funcion(Integer idFuncion) {
        this.idFuncion = idFuncion;
    }

    public Integer getIdFuncion() {
        return idFuncion;
    }

    public void setIdFuncion(Integer idFuncion) {
        this.idFuncion = idFuncion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @XmlTransient
    public List<Boleto> getBoletoList() {
        return boletoList;
    }

    public void setBoletoList(List<Boleto> boletoList) {
        this.boletoList = boletoList;
    }

    @XmlTransient
    public List<FuncionTipoBoleto> getFuncionTipoBoletoList() {
        return funcionTipoBoletoList;
    }

    public void setFuncionTipoBoletoList(List<FuncionTipoBoleto> funcionTipoBoletoList) {
        this.funcionTipoBoletoList = funcionTipoBoletoList;
    }

    public Pelicula getIdPelicula() {
        return idPelicula;
    }

    public void setIdPelicula(Pelicula idPelicula) {
        this.idPelicula = idPelicula;
    }

    public Sala getIdSala() {
        return idSala;
    }

    public void setIdSala(Sala idSala) {
        this.idSala = idSala;
    }

    public TipoFuncion getIdTipoFuncion() {
        return idTipoFuncion;
    }

    public void setIdTipoFuncion(TipoFuncion idTipoFuncion) {
        this.idTipoFuncion = idTipoFuncion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idFuncion != null ? idFuncion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Funcion)) {
            return false;
        }
        Funcion other = (Funcion) object;
        if ((this.idFuncion == null && other.idFuncion != null) || (this.idFuncion != null && !this.idFuncion.equals(other.idFuncion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cinearte.app.bean.Funcion[ idFuncion=" + idFuncion + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cinearte.app.bean;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author edivargas
 */
@Entity
@Table(name = "tipo_boleto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoBoleto.findAll", query = "SELECT t FROM TipoBoleto t")
    , @NamedQuery(name = "TipoBoleto.findByIdTipoBoleto", query = "SELECT t FROM TipoBoleto t WHERE t.idTipoBoleto = :idTipoBoleto")
    , @NamedQuery(name = "TipoBoleto.findByDescripcion", query = "SELECT t FROM TipoBoleto t WHERE t.descripcion = :descripcion")})
public class TipoBoleto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idTipoBoleto")
    private Integer idTipoBoleto;
    @Size(max = 45)
    @Column(name = "descripcion")
    private String descripcion;

    @OneToMany(mappedBy = "idTipoBoleto", fetch = FetchType.EAGER)
    private List<Precio> precioList;

    @OneToMany(mappedBy = "idTipoBoleto", fetch = FetchType.EAGER)
    private List<FuncionTipoBoleto> funcionTipoBoletoList;

    public TipoBoleto() {
    }

    public TipoBoleto(Integer idTipoBoleto) {
        this.idTipoBoleto = idTipoBoleto;
    }

    public Integer getIdTipoBoleto() {
        return idTipoBoleto;
    }

    public void setIdTipoBoleto(Integer idTipoBoleto) {
        this.idTipoBoleto = idTipoBoleto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public List<Precio> getPrecioList() {
        return precioList;
    }

    public void setPrecioList(List<Precio> precioList) {
        this.precioList = precioList;
    }

    @XmlTransient
    public List<FuncionTipoBoleto> getFuncionTipoBoletoList() {
        return funcionTipoBoletoList;
    }

    public void setFuncionTipoBoletoList(List<FuncionTipoBoleto> funcionTipoBoletoList) {
        this.funcionTipoBoletoList = funcionTipoBoletoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipoBoleto != null ? idTipoBoleto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoBoleto)) {
            return false;
        }
        TipoBoleto other = (TipoBoleto) object;
        if ((this.idTipoBoleto == null && other.idTipoBoleto != null) || (this.idTipoBoleto != null && !this.idTipoBoleto.equals(other.idTipoBoleto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cinearte.app.bean.TipoBoleto[ idTipoBoleto=" + idTipoBoleto + " ]";
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cinearte.app.dto;

import lombok.Data;

/**
 *
 * @author edivargas
 */
@Data
public class AsientoDTO {

    private String leyenda;
    private int fila;
    private int columna;
    private int estatus;

    public AsientoDTO(int fila, int columna, int estatus, String leyenda) {
        this.fila = fila;
        this.columna = columna;
        this.estatus = estatus;
        this.leyenda = leyenda;
    }

    @Override
    public String toString() {
        if (estatus == ASIENTO_VACIO) {
            return leyenda;
        } else if (estatus == ASIENTO_VENDIDO) {
            return "XX";
        } else if (estatus == ASIENTO_RESERVADO) {
            return "RR";
        } else {
            return "__";
        }
    }

    public static final int ASIENTO_VACIO = 0;
    public static final int ASIENTO_VENDIDO = 1;
    public static final int ASIENTO_RESERVADO = 2;
    public static final int ASIENTO_PASILLO = 3;
    public static final int ASIENTO_MINUSVALIDO = 4;
}

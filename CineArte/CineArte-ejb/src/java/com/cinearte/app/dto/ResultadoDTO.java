/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cinearte.app.dto;

import com.cinearte.app.util.CodigoResultado;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author edivargas
 */
@Getter
@Setter
@Data
public class ResultadoDTO<T> {
    
    public ResultadoDTO() {
        codigoResultado = CodigoResultado.OK;
    }

    private CodigoResultado codigoResultado;
    private String mensaje;
    private T resultado;

}

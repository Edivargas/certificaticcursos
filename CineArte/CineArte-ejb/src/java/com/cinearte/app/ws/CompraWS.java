/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cinearte.app.ws;

import com.cinearte.app.ejb.CompraEJB;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.jws.WebService;

/**
 *
 * @author edivargas
 */
@Stateless
@LocalBean
@WebService
public class CompraWS {
    
    @EJB
    CompraEJB compraEJB;
    
    public String compra() {
        return compraEJB.compra().toString();
    }
    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cinearte.app.rest;

import com.cinearte.app.bean.TipoSala;
import com.cinearte.app.ejb.CacheEJB;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletContext;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType; 
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

/**
 *
 * @author Richie Cedillo
 */
@Path("/catalogo")
@Produces(MediaType.APPLICATION_JSON)
public class CatalogoRS {

    @EJB
    CacheEJB cacheEJB;

    private ServletContext context;

//    @PostConstruct
//    private void init () {
//        String realPath = context.getRealPath("catalogo");
//    }
    @GET
    public Response getCatalogo() {
        ResponseBuilder responseBuilder = Response.status(Response.Status.FORBIDDEN);
        return responseBuilder.build();
    }
    
    @GET
    @Path("{tipoCatalogo}")
    public Object getCatalogos(@PathParam("tipoCatalogo") String tipoCatalogo) {
        switch (tipoCatalogo) {
            case "tipoSala":
                return cacheEJB.getTipoSalas();
            case "tipoBoleto":
                return cacheEJB.getTipoBoleto();
            case "actor":
                return cacheEJB.getActor();
            case "boleto":
                return cacheEJB.getBoleto();
            case "comentario":
                return cacheEJB.getComentario();
            default:
                ResponseBuilder responseBuilder = Response.status(Response.Status.BAD_REQUEST);
                return responseBuilder.build();
        }
    }
    
    @GET
    @Path("{tipoCatalogo}/{id}")
    public Object getCatalogos(@PathParam("tipoCatalogo") String tipoCatalogo,
            @PathParam("id") Long id) {
        switch (tipoCatalogo) {
            case "tipoSala":
                return cacheEJB.getTipoSalas();
            case "tipoBoleto":
                return cacheEJB.getTipoBoleto();
            case "actor":
                return cacheEJB.getActor();
            case "boleto":
                return cacheEJB.getBoleto();
            default:
                ResponseBuilder responseBuilder = Response.status(Response.Status.BAD_REQUEST);
                return responseBuilder.build();
        }
    }

    @POST
    @Path("/update")
    public String setCatalogo(@QueryParam("msg") String msg) {
        return ("respuesta POST -  msg: " + msg);
    }

    @DELETE
    @Path("/delete")
    public String borrarElemento(@QueryParam("id") String id) {
        System.out.println("Entré a delete");
        return "Se borró el elemento : " + id;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cinearte.app.dao;

import com.cinearte.app.bean.Actor;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * 
 * @author Richie Cedillo
 */
@Stateless
public class ActorDAO extends AbstractDAO<Actor> {

    @PersistenceContext(unitName = "CineArte-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ActorDAO() {
        super(Actor.class);
    }

}

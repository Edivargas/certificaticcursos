/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cinearte.app.dao;

import com.cinearte.app.bean.Boleto;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 * 
 * @author Richie Cedillo
 */
@Stateless
public class BoletoDAO extends AbstractDAO<Boleto> {

    @PersistenceContext(unitName = "CineArte-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BoletoDAO() {
        super(Boleto.class);
    }
    
    public List<Boleto> getByFuncion(int idFuncion){
        TypedQuery query=em.createQuery("SELECT b FROM Boleto b WHERE b.idFuncion.idFuncion=:idFuncion",Boleto.class);
        query.setParameter("idFuncion", idFuncion);
        return query.getResultList();
    }

}

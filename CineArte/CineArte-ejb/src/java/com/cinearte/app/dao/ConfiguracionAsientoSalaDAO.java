/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cinearte.app.dao;

import com.cinearte.app.bean.ConfiguracionAsientoSala;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 * 
 * @author Richie Cedillo
 */
@Stateless
public class ConfiguracionAsientoSalaDAO extends AbstractDAO<ConfiguracionAsientoSala> {

    @PersistenceContext(unitName = "CineArte-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ConfiguracionAsientoSalaDAO() {
        super(ConfiguracionAsientoSala.class);
    }
    
    public ConfiguracionAsientoSala getByIdSala(int idSala){
        TypedQuery query= em.createQuery("SELECT cas FROM ConfiguracionAsientoSala cas WHERE cas.idSala.idSala = :idSala",
                ConfiguracionAsientoSala.class);
        query.setParameter("idSala", idSala);
        return (ConfiguracionAsientoSala) query.getSingleResult();
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cinearte.app.dao;

import com.cinearte.app.bean.TipoFormaPago;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * 
 * @author Richie Cedillo
 */
@Stateless
public class TipoFormaPagoDAO extends AbstractDAO<TipoFormaPago> {

    @PersistenceContext(unitName = "CineArte-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TipoFormaPagoDAO() {
        super(TipoFormaPago.class);
    }

}

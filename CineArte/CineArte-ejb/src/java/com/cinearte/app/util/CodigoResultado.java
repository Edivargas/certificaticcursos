/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cinearte.app.util;

/**
 *
 * @author edivargas
 */
public enum CodigoResultado {

    OK (0, "ok"),
    PARAMETROS_INCOMPLETOS(-100, "Parametros Incompletos"),
    PARAMETROS_INCORRECTOS(-101, "Parametros Incorrectos"),
    ERROR_BASE_DATOS(-200, "error de base de datos");

    private int codigoError;

    private String mensaje;

    private CodigoResultado(int codigoError, String mensaje) {
        this.codigoError = codigoError;
        this.mensaje = mensaje;
    }

    public int getCodigoError() {
        return codigoError;
    }

    public String getMensaje() {
        return mensaje;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cinearte.app.ejb;

import com.cinearte.app.bean.Comentario;
import com.cinearte.app.dao.ComentarioDAO;
import com.cinearte.app.dto.ResultadoDTO;
import com.cinearte.app.util.CodigoResultado;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 *
 * @author lenovo
 */
@Stateless
@LocalBean
public class ComentarioEJB {

    @EJB
    ComentarioDAO comentarioDAO;

    @EJB
    CacheEJB cacheEJB;

    public ResultadoDTO agrega(String comentario, int calificacion) {
        ResultadoDTO resultado = new ResultadoDTO();
        if (comentario != null && comentario.isEmpty()
                && calificacion > 0 && calificacion < 5) {
            try {
                Comentario comentarioDTO = new Comentario();
                comentarioDTO.setComentario(comentario);
                comentarioDTO.setCalificacion(calificacion);
                comentarioDAO.create(comentarioDTO);
                cacheEJB.loadCatalogos();
            } catch (Exception e) {
                resultado.setCodigoResultado(CodigoResultado.ERROR_BASE_DATOS);
            }
        } else {
            resultado.setCodigoResultado(CodigoResultado.PARAMETROS_INCORRECTOS);

        }
        return resultado;
    }

}

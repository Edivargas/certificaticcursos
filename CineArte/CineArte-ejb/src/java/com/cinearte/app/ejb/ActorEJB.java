/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cinearte.app.ejb;

import com.cinearte.app.bean.Actor;
import com.cinearte.app.dao.ActorDAO;
import com.cinearte.app.dto.ResultadoDTO;
import com.cinearte.app.util.CodigoResultado;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 *
 * @author lenovo
 */
@Stateless
@LocalBean
public class ActorEJB {
    
    @EJB
    ActorDAO actorDAO;
    @EJB
    CacheEJB cacheEJB;
    
    public ResultadoDTO agrega(String nombre, Integer edad, String sexo, String foto ){
        ResultadoDTO resultado = new ResultadoDTO();
        if(nombre!= null && !nombre.isEmpty()
           && edad >0 && edad< 100
            && (sexo.equalsIgnoreCase("m") || sexo.equalsIgnoreCase("f"))  ) {
            try{
                Actor actor = new Actor();
                actor.setNombre(nombre);
                actor.setEdad(edad);
                actor.setSexo(sexo.toUpperCase());
                actor.setFoto(foto);
                actorDAO.create(actor);
                cacheEJB.loadCatalogos();
            }catch (Exception e){
                resultado.setCodigoResultado(CodigoResultado.ERROR_BASE_DATOS);
            }
        }else{
            resultado.setCodigoResultado(CodigoResultado.PARAMETROS_INCORRECTOS);
            
        }
            return resultado;
           
    }
    public ResultadoDTO modifica(Long id, String nombre, Integer edad, String sexo, String foto){
         ResultadoDTO resultado = new ResultadoDTO();
         if(id != null && id > 0 
             && nombre != null && !nombre.isEmpty()
               && edad > 0 && edad < 100
                 &&(sexo.equalsIgnoreCase("m") || sexo.equalsIgnoreCase("f"))){
             try{
                 Actor actor = actorDAO.find(id);
                  if(actor != null){
                     actor.setNombre(nombre);
                     actor.setEdad(edad);
                     actor.setSexo(sexo);
                     actor.setFoto(foto);
                     cacheEJB.loadCatalogos();
                  } else {
                     resultado.setCodigoResultado(CodigoResultado.PARAMETROS_INCORRECTOS);
                }
             }catch(Exception e){
                  resultado.setCodigoResultado(CodigoResultado.ERROR_BASE_DATOS);
                 
             }
        } else {
             resultado.setCodigoResultado(CodigoResultado.PARAMETROS_INCOMPLETOS);
         }
         return resultado;
    }
    public ResultadoDTO elimina(Long id){
        ResultadoDTO resultado = new ResultadoDTO();
        if(id != null && id > 0){
            actorDAO.remove(id);
            cacheEJB.loadCatalogos();
      } else {
            resultado.setCodigoResultado(CodigoResultado.PARAMETROS_INCORRECTOS);
        }
        return resultado;
    }
}   
    

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cinearte.app.ejb;

import com.cinearte.app.bean.Boleto;
import com.cinearte.app.dao.BoletoDAO;
import com.cinearte.app.dto.ResultadoDTO;
import com.cinearte.app.util.CodigoResultado;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;

/**
 *
 * @author edivargas
 */
@Stateless
@LocalBean
public class BoletoEJB {
    
    @EJB
    BoletoDAO boletoDAO;
   
    
    @EJB
    CacheEJB cacheEJB; 
    
    public ResultadoDTO agrega(String asiento){
        ResultadoDTO resultado = new ResultadoDTO();
        if(asiento != null && !asiento.isEmpty()){
         try{
             Boleto boleto = new Boleto();
             boleto.setAsiento(asiento);
             boletoDAO.create(boleto);
             cacheEJB.loadCatalogos();
         }catch (Exception e){
            resultado.setCodigoResultado(CodigoResultado.ERROR_BASE_DATOS);
          }  
       }else{
            resultado.setCodigoResultado(CodigoResultado.PARAMETROS_INCORRECTOS);
        }
        return resultado;
    }
    
    public ResultadoDTO modifica(Long id, String asiento){
        ResultadoDTO resultado = new ResultadoDTO();
        if(id != null && id > 0){
           try{
              Boleto boleto = boletoDAO.find(id);
              if(boleto != null){
                 boleto.setAsiento(asiento);
                 boletoDAO.edit(boleto);
                 cacheEJB.loadCatalogos();
              } else {
                  resultado.setCodigoResultado(CodigoResultado.PARAMETROS_INCORRECTOS);
             }
              }catch (Exception e){
                  resultado.setCodigoResultado(CodigoResultado.ERROR_BASE_DATOS);
           }
        } else {
            resultado.setCodigoResultado(CodigoResultado.PARAMETROS_INCOMPLETOS);
        }  
           return resultado;
        }
    public ResultadoDTO elimina(Long id){
        ResultadoDTO resultado = new ResultadoDTO();
        if (id != null && id > 0){
            boletoDAO.remove(id);
            cacheEJB.loadCatalogos();
        } else{
            resultado.setCodigoResultado(CodigoResultado.PARAMETROS_INCORRECTOS);
        }
        return resultado;
    }
}  

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cinearte.app.ejb;

import com.cinearte.app.bean.TipoSala;
import com.cinearte.app.dao.TipoSalaDAO;
import com.cinearte.app.dto.ResultadoDTO;
import com.cinearte.app.util.CodigoResultado;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;

/**
 *
 * @author edivargas
 */
@Stateless
@LocalBean
public class TipoSalaEJB {

    @EJB
    CacheEJB cacheEJB;
    @EJB
    TipoSalaDAO tipoSalaDAO;

    public ResultadoDTO agrega(String descripcion) {
        ResultadoDTO resultado = new ResultadoDTO();
        if (descripcion != null && !descripcion.isEmpty()) {
            try {
                TipoSala tipoSala = new TipoSala();
                tipoSala.setDescripcion(descripcion);
                tipoSalaDAO.create(tipoSala);
                cacheEJB.loadCatalogos();
            } catch (Exception e) {
                resultado.setCodigoResultado(CodigoResultado.ERROR_BASE_DATOS);
            }
        } else {
            resultado.
                    setCodigoResultado(CodigoResultado.PARAMETROS_INCORRECTOS);
        }
        return resultado;

    }

    public ResultadoDTO modifica(Long id, String descripcion) {
        ResultadoDTO resultado = new ResultadoDTO();
        if (id != null && id > 0) {
            try {
                TipoSala tipoSala = tipoSalaDAO.find(id);
                if (tipoSala != null) {
                    tipoSala.setDescripcion(descripcion);
                    tipoSalaDAO.edit(tipoSala);
                    cacheEJB.loadCatalogos();
                } else {
                    resultado.
                            setCodigoResultado(CodigoResultado.PARAMETROS_INCORRECTOS);
                }
            } catch (Exception e) {
                resultado.setCodigoResultado(CodigoResultado.ERROR_BASE_DATOS);
            }
        } else {
            resultado.
                    setCodigoResultado(CodigoResultado.PARAMETROS_INCORRECTOS);
        }
        return resultado;

    }

    public ResultadoDTO elimina(Long id) {
        ResultadoDTO resultado = new ResultadoDTO();
        if (id != null && id > 0) {
            tipoSalaDAO.remove(id);
            cacheEJB.loadCatalogos();
        } else {
            resultado.
                    setCodigoResultado(CodigoResultado.PARAMETROS_INCORRECTOS);
        }
        return resultado;
    }
}

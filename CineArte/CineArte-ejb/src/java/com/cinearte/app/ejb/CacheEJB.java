/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cinearte.app.ejb;

import com.cinearte.app.bean.Actor;
import com.cinearte.app.bean.Boleto;
import com.cinearte.app.bean.Comentario;
import com.cinearte.app.bean.TipoBoleto;
import com.cinearte.app.bean.TipoSala;
import com.cinearte.app.dao.ActorDAO;
import com.cinearte.app.dao.BoletoDAO;
import com.cinearte.app.dao.ComentarioDAO;
import com.cinearte.app.dao.TipoBoletoDAO;
import com.cinearte.app.dao.TipoSalaDAO;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.LocalBean;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Startup;
import javax.inject.Named;

/**
 *
 * @author edivargas
 */
@Singleton
@Startup
@LocalBean
@Named("cacheEJB")
public class CacheEJB {

    private final static Logger logger = Logger.getLogger("root");

    @EJB
    TipoSalaDAO tipoSalaDAO;

    @EJB
    TipoBoletoDAO tipoBoletoDAO;

    @EJB
    ActorDAO actorDAO;

    @EJB
    BoletoDAO boletoDAO;

    @EJB
    ComentarioDAO comentarioDAO;

    private final static List<TipoSala> tipoSalas = new ArrayList<>();
    private final static List<TipoBoleto> tipoBoletos = new ArrayList<>();
    private final static List<Actor> actores = new ArrayList<>();
    private final static List<Boleto> boletos = new ArrayList<>();
    private final static List<Comentario> comentarios = new ArrayList<>();

    @PostConstruct
    @Lock(LockType.WRITE)
    public void loadCatalogos() {
        System.out.println("ENTRANDO CACHE");
        try {
            tipoSalas.clear();
            tipoSalas.addAll(tipoSalaDAO.findAll());
            logger.info("Cargando TipoSalas: " + tipoSalas.size() + " cargados");
            tipoBoletos.clear();
            tipoBoletos.addAll(tipoBoletoDAO.findAll());
            logger.info("Cargando TipoBoletos: " + tipoBoletos.size() + " cargados");
            actores.clear();
            actores.addAll(actorDAO.findAll());
            logger.info("Cargando actores:" + actores.size() + " cargados ");
            boletos.clear();
            boletos.addAll(boletoDAO.findAll());
            logger.info("Cargando boletos:" + boletos.size() + " cargados ");
            comentarios.clear();
            comentarios.addAll(comentarioDAO.findAll());
            logger.info("Cargando comentarios:" + comentarios.size() + " cargados ");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    public <T extends Object> List<T> getCatalogo(T entityClass) {
        if (entityClass instanceof TipoSala) {
            return Collections.<T>unmodifiableList(tipoSalas);
        }
        return null;
    }
     */
    public List<TipoSala> getTipoSalas() {
        return Collections.unmodifiableList(tipoSalas);
    }

    public TipoSala getTipoSalaById(Long id) {
        for (TipoSala tipoSala : tipoSalas) {
            if (tipoSala.getIdTipoSala().equals(id)) {
                return tipoSala;
            }
        }
        return null;
    }

    public List<TipoBoleto> getTipoBoleto() {
        return Collections.unmodifiableList(tipoBoletos);
    }

    public List<Actor> getActor() {
        return Collections.unmodifiableList(actores);
    }

    public List<Boleto> getBoleto() {
        return Collections.unmodifiableList(boletos);
    }

    public List<Comentario> getComentario() {
        return Collections.unmodifiableList(comentarios);
    }
}
